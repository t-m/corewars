# CoreWars

This version of *CoreWars* is a fork of the (legacy) version 0.9.13 that can be found on
[CoreWars sourceforce project page](https://sourceforge.net/projects/corewars/).

About CoreWars see also:
https://corewars.org/


## Compilation / installation notes

### Prerequisites
Required libraries: **glib, gtk >= 2.0**


### Compilation/installation


#### Autotools

Execute `$ ./autogen.sh`, then standard autotools triplet:
`./configure; make ; make install`


#### Debian and other deb-based distros (suggested way)
For the moment - this is the most complete and easiest way to build the game.

Requirements:
- autotools
- Debian development tools: `devscripts`, `debhelper`, `dh-autoreconf`

To build source and `.deb` package just execute `tools/build_deb.sh`.
(Then as usual - install with `dpkg` / `apt`).
