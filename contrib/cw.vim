"
" Core Wars syntax file for vim
"
" March 2000, Alexey Vyskubov <alexey@byte-unix.piter-press.ru>
"
" Load this syntax file each time by typing ":source cw.vim" 
" or install it permanently (see ":help syntax.txt").
" 

syn clear
syn case match

syn keyword cwData	data
syn keyword cwCommand	move add neg and not movei
syn keyword cwFlow	loop jump fork
syn keyword cwCheck	own equal less
syn keyword cwSystem	info system

syn match	cwNumber	"-\=\<\d\+\>"
syn match	cwComment	"^#.*$"
syn match	cwTitle		"title.*$"

syn match	cwAddress	"\<[A-Z]\+[A-Z0-9]*\>"
syn match	cwRef		"&[A-Z0-9]\+\>"
syn region	cwRef2		start="\[" end="\]"

hi cwData	ctermfg=DarkGreen
hi cwCommand	ctermfg=Yellow
hi cwFlow	ctermfg=Green
hi cwCheck	ctermfg=Blue
hi cwSystem	ctermfg=Magenta
hi cwNumber	ctermfg=DarkGreen
hi cwComment	ctermfg=Cyan
hi cwTitle	ctermfg=Yellow ctermbg=DarkBlue
hi cwAddress	ctermfg=White
hi cwRef	ctermfg=LightBlue
hi cwRef2	ctermfg=DarkCyan


