This file describes the differences in the Redcode implementation
between this program and pMARS 0.8.0. Differences not mentioned here are
bugs. Please report them.

Walter Hofmann, March 2000

________________________________________
Opcodes:

LDP and STP are not recognised.

________________________________________
Pseudo opcodes:

Multi line EQU is not recognised. PIN is not recognised.

________________________________________
Modifiers:

All modifiers are recognised.

________________________________________
Addressing modes:

All addressing modes are recognised.

________________________________________
Directives:

Only ";name" and ";author" are recognised. All other directives are
ignored. Text before the redcode directive is not ignored.

________________________________________
Predefined variables:

Only CORESIZE, MAXPROCESSES, MAXCYCLES, MAXLENGTH, MINDISTANCE and
CURLINE are defined.

MINDISTANCE is minimum number of emty cells between the end of one 
program and the start of the next program. In pMARS it is the
minimum distance between the start of two programs.

________________________________________
Expression operators:

The following operators are NOT supported:

    Comparison:
        !=  inequality
        <   less than
        >   greater than
        <=  less than or equal
        >=  greater than or equal

    Logical:
        &&  and
        ||  or
        !   unary negation

    Assignment:
    =   (to register variables a..z)

Please note that two operators must not come directly after each other,
ie. 7*-5 is not allowed, use 7*(-5) or -7*5 instead.

________________________________________
Redcode grammar:

Stringisation is not supported. The right side of an EQU statement is
evaluated every time the label is used. It must evaluate to an
integer; no string substitution is done. EQU may be referenced before
they are defined. Multi line EQUs are not recognised. The last line in
a program (or the line with the END statement if there is one) must be
terminated with a newline. END and EQU statements are not allowed
within FOR blocks.
