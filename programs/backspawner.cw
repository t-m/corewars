	title	"Back Spawner"
	author	"Ron Steinke (rsteinke@u.washington.edu)"

# Spawns back movers.


	jump	BEGIN

LNGJMP:	fork	[SPNTO]
BEGIN:	move	4, COUNT
	move	&TO, FROM
	info	2, TO
	move	TO, SPNTO
	add	2, SPNTO
	jump	L
	data 0	# these are here
	data 0	# for spacing
	data 0
	data 0
	data 0
SPNTO:	data 0
TO:	data 0	# important! TO must come before COUNT
COUNT:	data 0	# (TO is modified first)
START:	move	TO, FROM
	add	-15, TO		# step must be <= -7 or >= 9,
L:	movei	FROM, TO	# must move LNGJMP to correspond
	movei	FROM, TO
	loop	COUNT, L
	jump	LNGJMP
FROM:	data 0
