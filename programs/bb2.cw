	  title "Black Beast 2"
	  author "Jacek Poplawski <jacekp@linux.com.pl>"

	  # get coresize
	  info 0,coresize
	  add -80,coresize
	  move &end,first
	  fork tail
	  
	  # ready scanner
readyscan:move first,scan
	  move coresize,steps
	  div step,steps
	  # scan
nextscan: add step,scan
	  equal empty,[scan]
	  jump skip
	  own [scan]
	  jump skip 

	  # bomb
	  add -5,scan
	  move 5,bombs
nextbomb: move empty,[scan]
	  add 2,scan
	  loop bombs,nextbomb

skip:	  loop steps,nextscan	  
endscan:  div 3,step
	  equal minstep,step
	  jump imp
	  jump readyscan
	  
first:	  data 0
scan:	  data 0
empty: 	  data 0
minstep:  data 1
step:	  data 243
coresize: data 0
steps:	  data 0
bombs:	  data 0
dst:	  data 0
forkbomb: fork forkbomb
forkbomb2: jump forkbomb
howmuch:  data 0
	  
tail:	  info 2,dst
	  own [dst]
	  jump tail
	  move forkbomb,[dst]
	  add 1,dst
	  move forkbomb2,[dst]
	  info 4,howmuch
	  equal 1,howmuch
imp:	  move imp,nextimp
nextimp:  jump tail	  
	
	  data 0
	  data 0 
	  data 0 
	  data 0 
	  data 0
	  data 0
	  data 0

end:	  data 0	  
	  
	  
	  
