	title	"Better Back"
	author	"Ron Steinke (rsteinke@u.washington.edu)"

# preliminary stuff

	move	&TO, TO
	move	4, COUNT
	jump	START

# sophisticated back mover

LNGJMP:	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
TO:	data 0	# important! TO must come before COUNT
COUNT:	data 0	# (TO is modified first)
START:	move	TO, FROM
	add	-15, TO		# step must be <= -7 or >= 9,
L:	movei	FROM, TO	# must move LNGJMP to correspond
	movei	FROM, TO
	loop	COUNT, L
	jump	LNGJMP
FROM:	data 0
