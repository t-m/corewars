#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Capture"
        author "Walter Hofmann <walterh@gmx.de>"

L:	info 2, C
	move C, D
	add 1, D
	own [C]
	jump L
	own [D]
	jump L
	move A, [C]
	move B, [D]
	jump L

JAR:	add 8, F
	move J4, [F]
	add -1, F
        move J3, [F]
	add -1, F
	move J2, [F]
	add -1, F
	move J1, [F]
	jump [F]
	
J1:	info 2, E
J2:	own [E]
J3:	move 0, [E]
J4:	jump J1
E:	data 0

A:	jump [B]
B:	data &JAR

C:	data 0
D:	data 0

F:	data &G
G:	data 0
