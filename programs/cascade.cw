#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Fork cascade"
        author "Walter Hofmann <walterh@gmx.de>"

	move I,L
	fork D
D:	fork E
E:      fork F
F:      fork G
G:      fork H
H:      fork I
I:	move K,I
M:	info 2,J
	move K,[J]
	move L,I
	jump [J]

J:	data 0
K:	jump K
L:	data 0

