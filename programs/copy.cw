#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Copy"
        author "Walter Hofmann <walterh@gmx.de>"

A:	data 0
B:	data 0
C:	data 0
D:	data 0
E:	data 0

START:	move &START, A
	info 2, B
	move B, D
	move 26, C

L:	own [B]
	jump START
	movei A, B
	loop C, L

CHECK:	move &START, A
	move D, B
	move 26, C

WAIT:	info 4, E
	less E, 2
	jump ACT
	own [A]
	jump OK1
	jump [D]
OK1:	own [B]
	jump OK2
	jump START
OK2:	add 1, A
	add 1, B
	loop C, WAIT
	jump CHECK

ACT:	fork [D]
	jump START
