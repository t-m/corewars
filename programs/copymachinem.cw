# Copy Machine (Server) Ver 0.1 for corewars
#
# This is the "Server" version of the "Copy Machine".only one copy of the master 
# should be running at a time. It accepts requests for replicating from 
# diffrent programs and transfers them to the so called "clients"

	Title "Copy Machine (Server)"
	Author "Idan Sofer <sofer@ibm.net>"
A:	data 120
B:      data 166
C:	data 0
D:	data 0
T:      data 0
CODE:	data 972
P:	data &P0
P0:	data 0	# Argument 1: Code
P1:	data 0	# Argument 2: Address
P2:	data 0	# Argument 3: Number of cells
P3:	data 0	# Argument 4: No. of first executive cell
P4:	data 0	# Argument 5: Where to put the jump/fork instruction
P5:	data 0 	# Argument 6: To fork?(0 = NO) # NOT YET IMPLEMENTED
L:	equal 0,[A]	# Wait for a request from another program
	jump L
	move [A],C
M:	equal 0,[B]	# Now, loop until we have a free client
	jump M
	move [B],D	
	move D,[C]	# Copy the address of the client's Comm-cell to the requesting proggie
	move 856,[D]	# Tell the client to stop reporting itself as free
	move 0,[A]
	move 0,[B]
	jump L
