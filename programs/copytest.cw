

	Title "Copy Machine Tester(2)"
	Author "Idan Sofer <sofer@ibm.net>"
A:	data 120
B:      data 0
C:	data 0
T:      data 4
CODE:	data 972
P:	data &P0
P0:	data 0	# Argument 1: Code
P1:	data 0	# Argument 2: Address
P2:	data 0	# Argument 3: Number of cells
P3:	data 0	# Argument 4: No. of first executive cell
P4:	data 0	# Argument 5: Where to put the jump/fork instruction
P5:	data 0 	# Argument 6: To fork?(0 = NO)
F:	move 0,B
G:	move &B,[A]
	equal B,0
	jump G
	move B,C
H:	loop T,H
L:	add 1,B
	move &A,[B]
	add 1,B
	move 30,[B]
	add 1,B
	move 12,[B]
	add 1,B
	move 29,[B]
	move C,B
	move CODE,[B]
M:	move 5,T
K:	jump K
