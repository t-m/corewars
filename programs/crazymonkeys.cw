#
# Core Wars program
#
# Walter Hofmann, March 2000
#

	title "Crazy Monkeys"
        author "Walter Hofmann <walterh@gmx.de>"

	info	2, DST
FC:	movei	SRC, DST
	loop	CNT, FC
	add	-11, DST
	fork	[DST]
	jump	ENTRY
	
START:	own	START
	data	0
	fork	START
	jump	START
THR:	data	0
ENTRY:	move	&START, SRC
	move	16, CNT
	info	2, DST
COPY:	movei	SRC, DST
	loop	CNT, COPY
	add	-11, DST
	info	4, THR
	less	12, THR
	jump	[DST]
	fork	[DST]
	jump	ENTRY
CNT:	data	16
SRC:	data	&START
DST:	data	0
