#
# A simple Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Faster memory eraser"
        author "Walter Hofmann <walterh@gmx.de>"

B:	data &D
START:	movei A, B	# select next cell
	movei A, B	# select next cell
	jump START	# an endless loop
A:	data &C
C:	data 0
D:	data 0
