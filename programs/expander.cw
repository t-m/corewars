#
# Core Wars program
#
# Daniel Karlsson, March 2000
#

	title "Expander"
        author "Daniel Karlsson <daniel.k@bigfoot.com>"

HEAD:	data	&HEAD
	add	-1, HEAD
	add	1, TAIL
EXP:	move	0, [HEAD]
	movei	HEAD, TAIL
	add	-2, HEAD
	jump	EXP
TAIL:	data	&TAIL
