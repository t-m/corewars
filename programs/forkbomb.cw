#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Forkbomb"
        author "Walter Hofmann <walterh@gmx.de>"

L:	info 2, B
	move A, [B]
	jump L

A:	fork A
B:	data 0
