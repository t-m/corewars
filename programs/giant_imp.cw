# Name    : Giant Imp 
# Author  : Jan Pool
# Version : 1.0.0 (04/03/2000)
#           A randition of the clasic Imp as decribed by corewars literature. 
#           In the original game only a kill counts, thus the number of cells
#           or time survided is irrelavent. This makes the Imp very useless. 
#           In this version of corewars the Imp (or self mover) is very 
#           effective. Although it doesn't kill anyone it just took all the 
#           cells:) An interesting effect is that it sometimes 'push' some of 
#           the processes infront of it.
#           This is called the Giant Imp because the program is twise as big
#           as the original Imp, basicly to fool the 'self mover' killing 
#           option. (I know it is nasty:)
#         : 1.0.1 (05/03/2000)
#           Changed the Labels
#           CS = Code segment
#           DS = Data segment
# Todo    : Nothing, I guess
# Note    : This is a self mover, so to see it work you have to enable this
#           class of warriors on the options menu.

	title "Giant Imp 1.0.0"
	author "Jan Pool <jpool@dsp.sun.ac.za>"

CS1:    move CS1,DS1
CS2:    move CS2,DS2
DS1:    data 0
DS2:    data 0
