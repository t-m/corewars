# "Santa's little helper", is the first proggie for corewars, AFAIK, that
# uses mechanism of "shared memory" for communication.
# The helper scans a specific cell(#59 in our case), and loops until it finds
# there a special ID code.then it goes to the next cell, which should contain
# an address where it should bind. 
# Then it copies there a fraction of code that is able to disable a wide range
# of approching programs, like "Walker" and "Selfmove", protecting another
# process which is too busy to deal with defence
#
# 

	Title "Santa's little helper"
	Author "Idan Sofer <sofer@ibm.net>"
	
A:	data 0
B:      data 59
C:	data 9
D:      data 0
CODE:	data 47295
L:	move [B],A
	equal A, CODE
	jump M
	jump L
M:	add 1,B
	move [B],A
	move 0,[B]
	move &END,D
COPY:	move [D],[A]
	add -1,C
	add -1,D
	add -1,A
	equal C,0
	jump FORKK
	jump COPY
FORKK:	add 3,A
	jump [A]
DATAS:	data 10
POINT:  data 0
P:	move 10,DATAS
	move &DATAS,POINT
	add -1,POINT
S:	move 0,[POINT]
	add -1,POINT
	loop DATAS,S
END:	jump P
