#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Hide bomb"
        author "Walter Hofmann <walterh@gmx.de>"

L:	info 2,A
	move B,[A]
	fork [A]
	loop C,L

A:	data 0
B:	jump B
C:	data 999
