#  This little proggie searchs for cells which are owned by someone
#  and takes the bet that they contain instructions and not data.
#  If lucky, threads will "ride" on code from other programs
#  while killing the original program

    Title "HitchHiker"
    Author "Idan Sofer <sofer@ibm.net>"
    
  
A:  data 0
AA: data 0
D:  data 0
E:  data 5
B:  info 2,A
    equal [A],AA
    jump B
    jump L
    jump B
L:  move [A], D
    move 0,[A]
M:  loop E,M
    move 5,E
    move D,[A]
    fork [A]
    jump B
