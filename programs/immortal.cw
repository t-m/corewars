#
# Core Wars program
#
# Mihai Roman, 16 Mar 2000
#
# Immortals - They are quite stupid... but there are lots of them :)))
#

	title "Immortal"
        author "Ratzalaru <ratzalaru@hotmail.com>"

S:	info 6, M
S1:	info 4, N
	info 2, L
	own L
	mul L, T4
	less N, M
	jump D
	jump S1
D:	move &S1, T1
	move L, T2
	move 19, T3
C:	movei T1, T2
	loop T3, C
	fork [L]
	info 2, L
	fork [L]
	own S1
	jump S1
M:	data 0
N:	data 0
L:	data 0
T1:	data 0
T2:	data 0
T3:	data 0
T4:	data 2
