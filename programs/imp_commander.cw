# Name    : Imp Commander
# Author  : Jan Pool
#           http://espresso.ee.sun.ac.za
#           jpool@dsp.sun.ac.za
# Version : 0.1.0 (04/03/2000)
#           First test version.
#           The idea was to make the 'Giant Imp' more robust, lettting it
#           survive longer. Imps are the fastest movers, but they are unable
#           to kill another process or protect itself from being killed. The
#           'Imp Commander' tries to ensure that the Imp stays allive longer by
#           deploying four processes each being a 'Giant Imp' on its own. The
#           program is very funerable in the depolyment stage (first 30 clock 
#           cycles). The processes is equaly distributed through the memory
#           and each moves at 1/4th the speed that a 'Giant Imp' does (since
#           the processing time is shared between the 4 threads. The
#           overall speed however is still the same, thus the 'Imp commander'
#           lose nothing in the line of speed. Another great advantage is
#           that with the threads distributed through the memory other
#           similar programs can only overwrite a fourty of the cells at a
#           time, but at a faster speed. Overall the 'Imp Commander' loose less
#           cells to memory erasers or other IMP:)
#         : 0.0.2 (05/03/2000)
#           Change label names
#           Decrease copy loop by 3 instructions, thus a 30% faster
#           deployment. 
#         : 0.0.3 (08/03/2000)
#           With the change of the 'self mover detection' logic I can just as
#           well use a normal imp, so I removed the 'Giant Imp' code and
#           replace it with an normal 'Imp'. I also used less data cells.
#           All in all I save a lot of instructions.
#         : 0.0.4 (09/03/2000)
#           Now that the mul and div instructions are added I can make sure
#           that this baby works well with all memory sized. 
#           Adds the calculation of the skip distance
# Todo    : Test for optimum number of Imps
# Note    : This is a self mover, so to see it work you have to enable this
#           class of warriors on the options menu.

        title "Imp Commander 0.0.4"
	author "Jan Pool <jpool@dsp.sun.ac.za>"

START:  info 0, SKIP                   # Get Memory Size
        div 4, SKIP                    # Calculate skip distance
COPY:   add SKIP, DST                  # Get next destination  
	move IMP, [DST]                # Copy Imp
	fork [DST]                     # Execute Imp
        loop COPIES,COPY               # Repeat
IMP:    move IMP, SKIP                 # Imp code
SKIP:   data 0                         # First hold mem size and then the skip distance
COPIES: data 3                         # Number of Imps - 1 (4)
DST:    data &DST                      # Destination for Imp
