#
# A simple Core Wars program
#
# Mihai Roman, March 2000
#

	title "Jump Back 2"
        author "Ratzalaru <ratzalaru@hotmail.com>"



T5:	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
T2:	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
T3:	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
T4:	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
T1:	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
S1:	move &T4, T1
        move &S2, T2
        move 6, T3
C1:     movei T2, T1
        loop T3, C1
	fork T4
S2:	move &T2, T1
        move &S2, T5
        move 6, T3
C2:	movei T5, T1
        loop T3, C2
	jump T2
