#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Jump up"
        author "Walter Hofmann <walterh@gmx.de>"

START:	move A, [B]
	add 1, B
	jump START

B:	data &A

JAR:	add -1, C
	move 0, [C]
	jump JAR

C:	data &START

UP:     jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
	jump JAR
A:	jump UP
