# Walter Hofmann
# March 2000

	title "Lethal"
	author "Walter Hofmann <walterh@gmx.de>"
	
pos:	data &pos
	fork clear
l:	add -2366, pos
	equal 0,[pos]
	jump l
	own [pos]
	add -2366,pos
found:	move pos,pos2
	move pos,pos3
	fork l3
	fork l
l2:	move fb,[pos2]
	add -1,pos2
	equal 0,[pos2]
pos2:	data 0
	jump l2
fb:	fork fb
l3:	move fb,[pos3]
	add 1,pos3
	equal 0,[pos3]
pos3:	data 0
	jump l3
a:	data &c
b:	data &d
clear:	movei a,b
	jump clear
c:	data 0
d:	data 0

