#
# Core Wars program
# a Runner borne by an Cannon
#
# by Sascha Wilde, 2000
#

	title "lhwShootingMother2"
    author "Sascha Wilde <swilde@gmx.de>"	

TARG:	data 0
START:	fork RUN
NEW:	move &TARG, TARG
	add -5, TARG
	move 8, COUNT
SHOOT:	add -1, TARG
	move 0, [TARG]
loop COUNT, SHOOT
	jump NEW
COUNT:	data 0
RUN:	move NEXT, B
NEXT:	move RUN, A
A:	data 0
B:	data 0
