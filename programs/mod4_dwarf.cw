# Name    : Modulo 4 Dwarf
# Author  : Jan Pool
# Version : 1.0.0 (03/03/2000)
#           The dwarf that Walter Hofmann wrote operated modulo 5, it
#           therefore moves faster through the memory area, but it ocupy
#           less cells if it survices. This warrior outperformed the mod 5
#           dwarf in all the test runs.

# Todo    : No imediate plans for this baby

        title "Modulo 4 Dwarf 1.0.0"
	author "Jan Pool <jpool@dsp.sun.ac.za>"

START:	add 4, END
	move 0, [END]
	jump START
END:	data &END
