#
# Alexey Vyskubov <alexey@pepper.spb.ru>, March 2000
#

title "Parasito Three"
author "Alexey Vyskubov <alexey@pepper.spb.ru>"

START:	fork P2
	fork P3
	fork P4
P1:	info 2, A1
	fork [A1]
	jump P1
A1:	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
P2:	info 2, A2
	fork [A2]
	jump P2
A2:	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
P3:	info 2, A3
	fork [A3]
	jump P3
A3:	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
P4:	info 2, A4
	fork [A4]
	jump P4
A4:	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
	data 0
