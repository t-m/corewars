#
# Alexey Vyskubov <alexey@pepper.spb.ru>, March 2000
#

title "Parasito Miner"
author "Alexey Vyskubov <alexey@pepper.spb.ru>"

START:	info 2, A
F:	equal [A], B
	jump G
	fork [A]
G:	add 4, A
	jump F
A:	data 0
B:	fork B
