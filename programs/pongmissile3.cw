#
# Pong Missile Launcher 3
#

title "Pong Missile 3"
author "Beno�t Timbert <Benoit.Timbert@enst-bretagne.fr>"

C:			data 0	# random choice
P:			data &C  # missile position
D:			data &E	# missile direction address
C2:		data 0	# 1/2 mem size
C4:		data 0	# 1/4 mem size
# directions
NE:		data 0
E:			data 1
SE:		data 0
S:			data 0
SO:		data 0
O:			data -1
NO:		data 0
N:			data 0
NE2:		data 0
E2:		data 1
SE2:		data 0
S2:		data 0
SO2:		data 0
O2:		data -1
NO2:		data 0
INIT:		info 1,S
			move S, S2
			neg S, N
			move S, SE
			add 1, SE
			move SE, SE2
			neg SE, NO
			move NO, NO2
			move S, SO
			add -1, SO
			move SO, SO2
			neg SO, NE
			move NE, NE2
			info 0, C2
			div 2, C2
			move C2, C4
			div 2, C4
MLOOP:	own [P]
			jump MMOVE
			info 2, [P]
MMOVE:	add [D], P
			equal [P], 0
			jump MLOOP
			info 2, C
			less C2, C
			jump MLOOP
			less C, C4
			jump SUB
			add 1,D
			equal	&INIT, D
			move &NE, D
			jump MLOOP
SUB:		add -1,D
			equal &C4, D
			move &NO2, D
			jump MLOOP		
			
			
