	title "Running Copy"
	author "Ron Steinke (rsteinke@u.washington.edu)"


TO:	data 0
FROM:	data &START

START:	info 2,TO
CONT:	move &START, FROM
	move 6, COUNT
L:	movei FROM, TO
	movei FROM, TO
	loop COUNT, L
	add -12, TO
	fork [TO]
	info 2,TO
	info 4,THRDS
	less THRDS, 15
	jump CONT

THRDS:	data 0
COUNT:	data 0
