#
# Core Wars program
#
# Walter Hofmann, March 2000
#

	title "Search & Destroy"
        author "Walter Hofmann <walterh@gmx.de>"

START:  info 2,A
	equal [A],B
	jump START
	move A,[C]
	add 1,C
	loop D,START
	move 15,D
L:	add -1,C
	move [C],A
	own [A]
	move &B,A	
	move B,[A]
	loop D,L
	move 15,D
	jump START
A:      data 0
B:      data 0
C:      data &E
D:	data 15
E:	data 0
