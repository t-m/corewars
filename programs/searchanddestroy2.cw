#
# Core Wars program
#
# Walter Hofmann, March 2000
#
# Modified by Idan Sofer(sofer@ibm.net)
# Additional features:
# * Plants fork bombs instead of zeros
# * Works with "Santa's little helper"

	title "Search & Destroy (2)"
        author "Idan Sofer <sofer@ibm.net>"

		
CODE:   data 47295
PRES:   move &CODE,[A]
        add -1,A
	move CODE,[A]
START:  info 2,A
	equal [A],B
	jump START
	move A,[C]
	add 1,C
	loop D,START
	move 15,D
L:	add -1,C
	move [C],A
	move 1,DD
M:	own [A]
	move &FO,A	
	move FO,[A]
	add 1,A
	add -1,DD
	equal [A],B
	jump N
N:	loop D,L
	move 15,D
	jump START
A:      data 60
B:      data 0
FO:     fork FO
C:      data &E
D:	data 15
DD:     data 5
E:	data 0
