#
# Core Wars program
#
# Static Chaos, 2000
# 
Title "Shooter"
Author "Static Chaos <schaos@freemail.hu>"

OFS:   data 0
DEST:  data 0
NEW:   data 0
CNT:   data 0
ENTRY: move &MID, DEST
       info 1, OFS
       move OFS, CNT
       add -1, CNT
       mul -1, OFS
NEXT:  add OFS, DEST
MID:   move 0, [DEST]
       loop CNT, NEXT
SPAWN: move &ENTRY, OFS
       info 2, DEST
       move DEST, NEW
       move 8, CNT
SP2:   movei OFS, DEST
       movei OFS, DEST
       loop CNT, SP2
       jump [NEW]
