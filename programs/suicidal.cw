#
# Core Wars program
#
# Daniel Karlsson, March 2000
#

	title "Suicidal Quintet"
        author "Daniel Karlsson <daniel.k@bigfoot.com>"

	move	4, D
FORK1:	move	&START, A
	move	3, B
	info	2, C
	move	C, E
COPY:	movei	A, C
	loop	B, COPY
	fork	[E]
	loop	D, FORK1
START:	info	2, A
	move	0, [A]
	jump	START
A:	data	0
B:	data	0
C:	data	0
D:	data	0
E:	data	0
