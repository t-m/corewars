#
# Core Wars program
#
# Modified version of teleporter by Daniel Karlsson, March 2000
#

	title "Teleporter2"
	author "fzago <fzago@gresham-computing.com>"

START:	info	2, LOC
		move	10, CNT
		move	&START, SRC
		move	LOC, DST
COPY:	own	[DST]
		jump	START
		movei	SRC, DST
		loop	CNT, COPY
		fork	[LOC]	# Insert Star Trek teleporter sound here
		jump START

LOC:	data	0
SRC:	data	0
DST:	data	0
CNT:	data	0
