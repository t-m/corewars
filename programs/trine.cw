
#
# By Zinx Verituse <zinx@linuxfreak.com>
#
#  Trine

title "Trine"
author "Zinx Verituse <zinx@linuxfreak.com>"

DUMMY1:	data 0
A:	data &DUMMY2

START:	fork LEFT

RIGHT:	move 0, [A]
	add 3, A
	jump RIGHT

LEFT:	move 0, [B]
	add -3, B
	jump LEFT

B:	data &DUMMY1
DUMMY2:	data 0
