#
# Core Wars program
#
# Walter Hofmann, May 1999
#

	title "Walker"
        author "Walter Hofmann <walterh@gmx.de>"

START:  move &START, A
	move &END, B
	move 6, C
L:	movei A, B
	loop C, L
	jump END
A:      data 0
B:      data 0
C:      data 0
END:	data 0
