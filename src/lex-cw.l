/*  Core Wars.
 *  Copyright (C) 1999  Walter Hofmann
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as 
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

%{
#include <string.h>

#include "parser-cw.h"
#include "program.h"
%}

%option noyywrap

%%

title			cw_lval.integer = CMD_TITLE; return KEYWORD;
author			cw_lval.integer = CMD_AUTHOR; return KEYWORD;
data			cw_lval.integer = CMD_DATA; return KEYWORD;
move			cw_lval.integer = CMD_MOVE; return KEYWORD;
add			cw_lval.integer = CMD_ADD; return KEYWORD;
neg			cw_lval.integer = CMD_NEG; return KEYWORD;
mul			cw_lval.integer = CMD_MUL; return KEYWORD;
div			cw_lval.integer = CMD_DIV; return KEYWORD;
mod			cw_lval.integer = CMD_MOD; return KEYWORD;
and			cw_lval.integer = CMD_AND; return KEYWORD;
not			cw_lval.integer = CMD_NOT; return KEYWORD;
equal			cw_lval.integer = CMD_EQUAL; return KEYWORD;
less			cw_lval.integer = CMD_LESS; return KEYWORD;
jump			cw_lval.integer = CMD_JUMP; return KEYWORD;
fork			cw_lval.integer = CMD_FORK; return KEYWORD;
info			cw_lval.integer = CMD_INFO; return KEYWORD;
system			cw_lval.integer = CMD_SYSTEM; return KEYWORD;
own			cw_lval.integer = CMD_OWN; return KEYWORD;
loop			cw_lval.integer = CMD_LOOP; return KEYWORD;
movei			cw_lval.integer = CMD_MOVEI; return KEYWORD;
[0-9]+			cw_lval.integer = atoi (cw_text); return INTEGER;
-[0-9]+			cw_lval.integer = atoi (cw_text); return INTEGER;
[[:alpha:]][[:alnum:]]*	{
			  cw_lval.name = strncpy (malloc (cw_leng+1), cw_text, cw_leng); 
			  cw_lval.name[cw_leng] = 0; 
			  return IDENTIFIER;
			}
[[:blank:]]+		return WHITESPACE;
"#"[^\n]*		return COMMENT;
\"[^\n]*\"		{
			  cw_lval.name = strncpy (malloc (cw_leng-1), cw_text+1, cw_leng-2); 
			  cw_lval.name[cw_leng-2] = 0; 
			  return STRING;
			}
<<EOF>>			return 0;
.|\n			return *cw_text;
