/*  Core Wars.
 *  Copyright (C) 1999  Walter Hofmann
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as 
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

%{
#include <string.h>

#include "parser-rc.h" 
#include "program-rc.h" 
%}

%option noyywrap

%%

ORG				return RC_INSTR_ORG;
EQU				return RC_INSTR_EQU;
END				return RC_INSTR_END;
FOR				return RC_INSTR_FOR;
ROF				return RC_INSTR_ROF;

DAT(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_DAT; REJECT;
MOV(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_MOV; REJECT;
ADD(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_ADD; REJECT;
SUB(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_SUB; REJECT;
MUL(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_MUL; REJECT;
DIV(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_DIV; REJECT;
MOD(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_MOD; REJECT;
JMP(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_JMP; REJECT;
JMZ(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_JMZ; REJECT;
JMN(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_JMN; REJECT;
DJN(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_DJN; REJECT;
CMP(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_CMP; REJECT;
SLT(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_SLT; REJECT;
SPL(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_SPL; REJECT;
SEQ(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_SEQ; REJECT;
SNE(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_SNE; REJECT;
NOP(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_NOP; REJECT;
IJZ(\.(A|B|AB|BA|F|X|I))?	rc_lval.integer = RC_CMD_IJZ; REJECT;

(DAT|MOV|ADD|SUB|MUL|DIV|MOD|JMP|JMZ|JMN|DJN|CMP|SLT|SPL|SEQ|SNE|NOP|IJZ)\.A  rc_lval.integer |= RC_MOD_A; return RC_CMD; 
(DAT|MOV|ADD|SUB|MUL|DIV|MOD|JMP|JMZ|JMN|DJN|CMP|SLT|SPL|SEQ|SNE|NOP|IJZ)\.B  rc_lval.integer |= RC_MOD_B; return RC_CMD; 
(DAT|MOV|ADD|SUB|MUL|DIV|MOD|JMP|JMZ|JMN|DJN|CMP|SLT|SPL|SEQ|SNE|NOP|IJZ)\.AB rc_lval.integer |= RC_MOD_AB; return RC_CMD; 
(DAT|MOV|ADD|SUB|MUL|DIV|MOD|JMP|JMZ|JMN|DJN|CMP|SLT|SPL|SEQ|SNE|NOP|IJZ)\.BA rc_lval.integer |= RC_MOD_BA; return RC_CMD; 
(DAT|MOV|ADD|SUB|MUL|DIV|MOD|JMP|JMZ|JMN|DJN|CMP|SLT|SPL|SEQ|SNE|NOP|IJZ)\.F  rc_lval.integer |= RC_MOD_F; return RC_CMD; 
(DAT|MOV|ADD|SUB|MUL|DIV|MOD|JMP|JMZ|JMN|DJN|CMP|SLT|SPL|SEQ|SNE|NOP|IJZ)\.X  rc_lval.integer |= RC_MOD_X; return RC_CMD; 
(DAT|MOV|ADD|SUB|MUL|DIV|MOD|JMP|JMZ|JMN|DJN|CMP|SLT|SPL|SEQ|SNE|NOP|IJZ)\.I  rc_lval.integer |= RC_MOD_I; return RC_CMD; 
(DAT|MOV|ADD|SUB|MUL|DIV|MOD|JMP|JMZ|JMN|DJN|CMP|SLT|SPL|SEQ|SNE|NOP|IJZ)     return RC_CMD;

[0-9]+				rc_lval.integer = atoi (rc_text); return INTEGER;

[[:alpha:]_][[:alnum:]_]*	{
				  rc_lval.name = strncpy (malloc (rc_leng+1), rc_text, rc_leng); 
				  rc_lval.name[rc_leng] = 0; 
				  return IDENTIFIER;
				}

[[:blank:]]+			return WHITESPACE;
";"[^\n]*			return COMMENT;
<<EOF>>				return 0;
.|\n				return *rc_text;
