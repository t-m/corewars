/*  Core Wars.
 *  Copyright (C) 1999  Walter Hofmann
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as 
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __MAIN_H__
#define __MAIN_H__

#include <stdio.h>

#ifdef DEBUG1
#define DEBUG1_PRINTF printf
#else
#define DEBUG1_PRINTF
#endif

#ifdef DEBUG2
#define DEBUG2_PRINTF printf
#else
#define DEBUG2_PRINTF
#endif

#define FAIL 0
#define OK 1

#define CW_PROGRAM_FILE_EXTENSION ".cw"
#define CW_PROGRAM_FILE_EXTENSION_LENGTH 3
#define RC_PROGRAM_FILE_EXTENSION ".red"
#define RC_PROGRAM_FILE_EXTENSION_LENGTH 4

typedef int bool;

extern FILE *yyin;
extern int yydebug;

char *itos (int i);

#endif
