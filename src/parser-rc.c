/* A Bison parser, made from parser-rc.y
   by GNU bison 1.35.  */

#define YYBISON 1  /* Identify Bison output.  */

#define yyparse rc_parse
#define yylex rc_lex
#define yyerror rc_error
#define yylval rc_lval
#define yychar rc_char
#define yydebug rc_debug
#define yynerrs rc_nerrs
# define	INTEGER	257
# define	IDENTIFIER	258
# define	WHITESPACE	259
# define	COMMENT	260
# define	RC_CMD	261
# define	RC_INSTR_EQU	262
# define	RC_INSTR_ORG	263
# define	RC_INSTR_END	264
# define	RC_INSTR_FOR	265
# define	RC_INSTR_ROF	266

#line 18 "parser-rc.y"


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <ctype.h>

#include "main.h"
#include "options.h"
#include "program.h"
#include "program-rc.h"

#define cmalloc(x) calloc(1,x)

#define EXPR_INTEGER	0 /* integer         */
#define EXPR_LABEL	1 /* label           */
#define EXPR_ADD	2 /* integer * a + b */
#define EXPR_SUB	3 /* integer * a - b */
#define EXPR_SGN	4 /* integer * a     */
#define EXPR_MUL	5 /* a * b           */
#define EXPR_DIV	6 /* a / b           */
#define EXPR_MOD	7 /* a % b           */
#define EXPR_EQU	8 /* a == b          */

  struct line {
    struct line *next;
    struct rc_command *cmd;
  } ;
  
  struct expr {
    int what;
    int integer;
    char *name;
    int operation;
    struct expr *a, *b;      
  } ;    

  struct label {
    struct label *next;
    char *name;
    int relative;
    int address;
    int in_eval;
    struct expr *expr;
  } ;

  struct label *create_label(char *name);
  void free_expr(struct expr *expr);
  struct expr *copy_expr(struct expr *expr);

  int current_line, *current_cell;
  struct line *root_line;
  struct expr *org;
  struct label *label_list;
  struct label *for_index;

  char rc_error_message[1024];

  int eval_expr(struct expr *expr, int rel, jmp_buf env);
  void subs_expr(struct expr *expr, struct label *label, int i);
  void rc_error (char *s);
  int rc_lex (void);
  void rc_restart(FILE *input_file);

#line 84 "parser-rc.y"
#ifndef YYSTYPE
typedef union {
  int integer;
  char *name;
  struct rc_command *cmd;
  struct line *line;
  struct label *label;
  struct expr *expr;
} yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif
#ifndef YYDEBUG
# define YYDEBUG 1
#endif



#define	YYFINAL		133
#define	YYFLAG		-32768
#define	YYNTBASE	31

/* YYTRANSLATE(YYLEX) -- Bison token number corresponding to YYLEX. */
#define YYTRANSLATE(x) ((unsigned)(x) <= 266 ? yytranslate[x] : 56)

/* YYTRANSLATE[YYLEX] -- Bison token number corresponding to YYLEX. */
static const char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      13,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,    16,    17,    28,     2,     2,
      29,    30,    23,    25,    14,    26,     2,    27,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    15,     2,
      19,    24,    20,     2,    18,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    21,     2,    22,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12
};

#if YYDEBUG
static const short yyprhs[] =
{
       0,     0,     1,     5,     6,     8,    10,    13,    16,    18,
      21,    25,    30,    31,    33,    35,    37,    39,    41,    47,
      57,    60,    61,    63,    69,    74,    75,    78,    82,    83,
      86,    89,    92,    95,    98,   101,   104,   107,   109,   115,
     117,   119,   121,   126,   131,   136,   141,   145,   149,   151,
     156,   161,   166,   169,   172,   178,   186,   193,   198,   202,
     203,   204
};
static const short yyrhs[] =
{
      -1,    32,    33,    34,     0,     0,     5,     0,    36,     0,
      34,    36,     0,    34,    50,     0,    36,     0,    34,    36,
       0,    37,    13,    33,     0,    38,    37,    13,    33,     0,
       0,     6,     0,    51,     0,    52,     0,    53,     0,    39,
       0,    40,     7,    33,    43,    44,     0,    40,     7,    33,
      43,    44,    14,    33,    43,    44,     0,    42,    41,     0,
       0,    42,     0,    42,    37,    13,    33,    41,     0,    37,
      13,    33,    41,     0,     0,     4,    33,     0,     4,    15,
      33,     0,     0,    16,    33,     0,    17,    33,     0,    18,
      33,     0,    19,    33,     0,    20,    33,     0,    21,    33,
       0,    22,    33,     0,    23,    33,     0,    45,     0,    45,
      24,    24,    33,    45,     0,    46,     0,    47,     0,    48,
       0,    46,    25,    33,    48,     0,    46,    26,    33,    48,
       0,    25,    33,    48,    47,     0,    26,    33,    48,    47,
       0,    25,    33,    48,     0,    26,    33,    48,     0,    49,
       0,    48,    23,    33,    49,     0,    48,    27,    33,    49,
       0,    48,    28,    33,    49,     0,     4,    33,     0,     3,
      33,     0,    29,    33,    44,    30,    33,     0,    40,    10,
      33,    44,    37,    13,    33,     0,    40,    10,    33,    37,
      13,    33,     0,    42,     8,    33,    44,     0,     9,    33,
      44,     0,     0,     0,    40,    54,    11,    33,    44,    37,
      13,    33,    55,    35,    12,    33,     0
};

#endif

#if YYDEBUG
/* YYRLINE[YYN] -- source line where rule number YYN was defined. */
static const short yyrline[] =
{
       0,   115,   115,   120,   121,   125,   127,   143,   148,   150,
     168,   170,   175,   176,   180,   182,   185,   188,   193,   211,
     221,   223,   227,   229,   232,   235,   239,   248,   260,   262,
     265,   268,   271,   274,   277,   280,   283,   288,   290,   300,
     302,   307,   309,   318,   329,   337,   346,   354,   364,   366,
     374,   382,   392,   398,   405,   416,   418,   423,   427,   431,
     431,   431
};
#endif


#if (YYDEBUG) || defined YYERROR_VERBOSE

/* YYTNAME[TOKEN_NUM] -- String name of the token TOKEN_NUM. */
static const char *const yytname[] =
{
  "$", "error", "$undefined.", "INTEGER", "IDENTIFIER", "WHITESPACE", 
  "COMMENT", "RC_CMD", "RC_INSTR_EQU", "RC_INSTR_ORG", "RC_INSTR_END", 
  "RC_INSTR_FOR", "RC_INSTR_ROF", "'\\n'", "','", "':'", "'#'", "'$'", 
  "'@'", "'<'", "'>'", "'{'", "'}'", "'*'", "'='", "'+'", "'-'", "'/'", 
  "'%'", "'('", "')'", "program", "@1", "whitespace", "list", "for_list", 
  "line", "comment", "instruction", "command", "label_list", 
  "label_list_ne", "label", "address_mode", "expr", "expr0", "expr1", 
  "expr1s", "expr2", "expr3", "instr_end", "instr_equ", "instr_org", 
  "instr_for", "@2", "@3", 0
};
#endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives. */
static const short yyr1[] =
{
       0,    32,    31,    33,    33,    34,    34,    34,    35,    35,
      36,    36,    37,    37,    38,    38,    38,    38,    39,    39,
      40,    40,    41,    41,    41,    41,    42,    42,    43,    43,
      43,    43,    43,    43,    43,    43,    43,    44,    44,    45,
      45,    46,    46,    46,    47,    47,    47,    47,    48,    48,
      48,    48,    49,    49,    49,    50,    50,    51,    52,    54,
      55,    53
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN. */
static const short yyr2[] =
{
       0,     0,     3,     0,     1,     1,     2,     2,     1,     2,
       3,     4,     0,     1,     1,     1,     1,     1,     5,     9,
       2,     0,     1,     5,     4,     0,     2,     3,     0,     2,
       2,     2,     2,     2,     2,     2,     2,     1,     5,     1,
       1,     1,     4,     4,     4,     4,     3,     3,     1,     4,
       4,     4,     2,     2,     5,     7,     6,     4,     3,     0,
       0,    12
};

/* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
   doesn't specify something else to do.  Zero means the default is an
   error. */
static const short yydefact[] =
{
       1,     3,     4,    21,     3,    13,     3,    21,     5,     0,
      12,    17,    59,    25,    14,    15,    16,     3,    26,     0,
       6,    59,     7,     3,     0,     3,     0,     3,     0,    20,
      22,    27,     3,     3,     3,     3,     3,    58,    37,    39,
      40,    41,    48,     3,    10,     3,    28,     3,     0,     3,
       0,    53,    52,     0,     0,     0,     0,     3,     3,     3,
       3,     3,    12,    11,     3,     3,     3,     3,     3,     3,
       3,     3,     0,     0,    57,    25,     3,    46,    47,     0,
       3,     0,     0,     0,     0,     0,     0,    12,    29,    30,
      31,    32,    33,    34,    35,    36,    18,    12,    24,    25,
      44,    45,     3,     0,    42,    43,    49,    50,    51,     3,
       0,     3,     0,    23,    54,    38,    56,     3,    28,     3,
      55,     0,    60,    19,    21,    21,     0,     5,     6,     3,
      61,     0,     0,     0
};

static const short yydefgoto[] =
{
     131,     1,     3,     7,   126,     8,     9,    10,    11,    12,
      29,    13,    72,    37,    38,    39,    40,    41,    42,    22,
      14,    15,    16,    26,   124
};

static const short yypact[] =
{
  -32768,     5,-32768,   123,     0,-32768,     5,   117,-32768,    -1,
       8,-32768,    15,    34,-32768,-32768,-32768,     5,-32768,    83,
  -32768,    63,-32768,     5,    13,     5,    23,     5,    37,-32768,
       3,-32768,     5,     5,     5,     5,     5,-32768,    22,    33,
  -32768,    21,-32768,     5,-32768,     5,   121,     5,    83,     5,
      58,-32768,-32768,     4,     4,    83,    54,     5,     5,     5,
       5,     5,    71,-32768,     5,     5,     5,     5,     5,     5,
       5,     5,    83,    83,-32768,    77,     5,   122,   122,    55,
       5,     4,     4,     4,     4,     4,    78,     8,-32768,-32768,
  -32768,-32768,-32768,-32768,-32768,-32768,    10,     8,-32768,    77,
  -32768,-32768,     5,    83,    21,    21,-32768,-32768,-32768,     5,
      80,     5,    82,-32768,-32768,-32768,-32768,     5,   121,     5,
  -32768,    83,-32768,-32768,   123,   123,    87,    98,    99,     5,
  -32768,   114,   116,-32768
};

static const short yypgoto[] =
{
  -32768,-32768,    -4,     9,-32768,    -6,     7,-32768,-32768,    -3,
     -72,    -7,     2,   -37,    25,-32768,    11,    -2,    18,-32768,
  -32768,-32768,-32768,-32768,-32768
};


#define	YYLAST		150


static const short yytable[] =
{
      18,    20,    19,    98,    21,     2,    30,    32,    33,     5,
       2,    74,    23,    31,     5,    17,   -12,    24,    79,    44,
      28,    46,    25,    48,   111,    87,    45,   113,    51,    52,
      53,    54,    55,    36,    47,    96,    97,    50,     4,    62,
       5,    63,    27,    73,    59,    75,    56,   -12,    60,    61,
      49,    77,    78,    81,    82,    83,    84,    85,    57,    58,
      88,    89,    90,    91,    92,    93,    94,    95,    30,    86,
      25,    76,    99,    43,    32,    33,   103,     5,    80,   104,
     105,     4,    28,     5,   123,   102,    32,    33,   100,   101,
     -12,   109,    30,   117,   110,   119,    34,    35,   114,   129,
      36,   106,   107,   108,   112,   116,    28,   118,    34,    35,
      -8,    -9,    36,   120,   132,   122,   133,    -2,   127,   128,
     121,     4,    21,     5,     0,   130,     6,     4,   115,     5,
     -12,     0,     6,   125,     0,     0,   -12,    64,    65,    66,
      67,    68,    69,    70,    71,    59,     0,    34,    35,    60,
      61
};

static const short yycheck[] =
{
       4,     7,     6,    75,     7,     5,    13,     3,     4,     6,
       5,    48,    13,    17,     6,    15,    13,    10,    55,    23,
      13,    25,     7,    27,    14,    62,    13,    99,    32,    33,
      34,    35,    36,    29,    11,    72,    73,    30,     4,    43,
       6,    45,     8,    47,    23,    49,    24,    13,    27,    28,
      13,    53,    54,    57,    58,    59,    60,    61,    25,    26,
      64,    65,    66,    67,    68,    69,    70,    71,    75,    62,
       7,    13,    76,    10,     3,     4,    80,     6,    24,    81,
      82,     4,    75,     6,   121,    30,     3,     4,    77,    78,
      13,    13,    99,    13,    87,    13,    25,    26,   102,    12,
      29,    83,    84,    85,    97,   109,    99,   111,    25,    26,
      12,    12,    29,   117,     0,   119,     0,     0,   124,   125,
     118,     4,   125,     6,    -1,   129,     9,     4,   103,     6,
      13,    -1,     9,   124,    -1,    -1,    13,    16,    17,    18,
      19,    20,    21,    22,    23,    23,    -1,    25,    26,    27,
      28
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "/usr/share/bison/bison.simple"

/* Skeleton output parser for bison,

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software
   Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* This is the parser code that is written into each bison parser when
   the %semantic_parser declaration is not specified in the grammar.
   It was written by Richard Stallman by simplifying the hairy parser
   used when %semantic_parser is specified.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

#if ! defined (yyoverflow) || defined (YYERROR_VERBOSE)

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# if YYSTACK_USE_ALLOCA
#  define YYSTACK_ALLOC alloca
# else
#  ifndef YYSTACK_USE_ALLOCA
#   if defined (alloca) || defined (_ALLOCA_H)
#    define YYSTACK_ALLOC alloca
#   else
#    ifdef __GNUC__
#     define YYSTACK_ALLOC __builtin_alloca
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC malloc
#  define YYSTACK_FREE free
# endif
#endif /* ! defined (yyoverflow) || defined (YYERROR_VERBOSE) */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (YYLTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
# if YYLSP_NEEDED
  YYLTYPE yyls;
# endif
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAX (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# if YYLSP_NEEDED
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE) + sizeof (YYLTYPE))	\
      + 2 * YYSTACK_GAP_MAX)
# else
#  define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAX)
# endif

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAX;	\
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif


#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	goto yyacceptlab
#define YYABORT 	goto yyabortlab
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");			\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).

   When YYLLOC_DEFAULT is run, CURRENT is set the location of the
   first token.  By default, to implement support for ranges, extend
   its range to the last symbol.  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)       	\
   Current.last_line   = Rhs[N].last_line;	\
   Current.last_column = Rhs[N].last_column;
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#if YYPURE
# if YYLSP_NEEDED
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, &yylloc, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval, &yylloc)
#  endif
# else /* !YYLSP_NEEDED */
#  ifdef YYLEX_PARAM
#   define YYLEX		yylex (&yylval, YYLEX_PARAM)
#  else
#   define YYLEX		yylex (&yylval)
#  endif
# endif /* !YYLSP_NEEDED */
#else /* !YYPURE */
# define YYLEX			yylex ()
#endif /* !YYPURE */


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)
/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
#endif /* !YYDEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif

#ifdef YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif
#endif

#line 315 "/usr/share/bison/bison.simple"


/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
#  define YYPARSE_PARAM_ARG void *YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL
# else
#  define YYPARSE_PARAM_ARG YYPARSE_PARAM
#  define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
# endif
#else /* !YYPARSE_PARAM */
# define YYPARSE_PARAM_ARG
# define YYPARSE_PARAM_DECL
#endif /* !YYPARSE_PARAM */

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
# ifdef YYPARSE_PARAM
int yyparse (void *);
# else
int yyparse (void);
# endif
#endif

/* YY_DECL_VARIABLES -- depending whether we use a pure parser,
   variables are global, or local to YYPARSE.  */

#define YY_DECL_NON_LSP_VARIABLES			\
/* The lookahead symbol.  */				\
int yychar;						\
							\
/* The semantic value of the lookahead symbol. */	\
YYSTYPE yylval;						\
							\
/* Number of parse errors so far.  */			\
int yynerrs;

#if YYLSP_NEEDED
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES			\
						\
/* Location data for the lookahead symbol.  */	\
YYLTYPE yylloc;
#else
# define YY_DECL_VARIABLES			\
YY_DECL_NON_LSP_VARIABLES
#endif


/* If nonreentrant, generate the variables here. */

#if !YYPURE
YY_DECL_VARIABLES
#endif  /* !YYPURE */

int
yyparse (YYPARSE_PARAM_ARG)
     YYPARSE_PARAM_DECL
{
  /* If reentrant, generate the variables here. */
#if YYPURE
  YY_DECL_VARIABLES
#endif  /* !YYPURE */

  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yychar1 = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack. */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;

#if YYLSP_NEEDED
  /* The location stack.  */
  YYLTYPE yylsa[YYINITDEPTH];
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;
#endif

#if YYLSP_NEEDED
# define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
# define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  YYSIZE_T yystacksize = YYINITDEPTH;


  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
#if YYLSP_NEEDED
  YYLTYPE yyloc;
#endif

  /* When reducing, the number of symbols on the RHS of the reduced
     rule. */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;
#if YYLSP_NEEDED
  yylsp = yyls;
#endif
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  */
# if YYLSP_NEEDED
	YYLTYPE *yyls1 = yyls;
	/* This used to be a conditional around just the two extra args,
	   but that might be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);
	yyls = yyls1;
# else
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);
# endif
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);
# if YYLSP_NEEDED
	YYSTACK_RELOCATE (yyls);
# endif
# undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
#if YYLSP_NEEDED
      yylsp = yyls + yysize - 1;
#endif

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yychar1 = YYTRANSLATE (yychar);

#if YYDEBUG
     /* We have to keep this `#if YYDEBUG', since we use variables
	which are defined only if `YYDEBUG' is set.  */
      if (yydebug)
	{
	  YYFPRINTF (stderr, "Next token is %d (%s",
		     yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise
	     meaning of a token, for further debugging info.  */
# ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
# endif
	  YYFPRINTF (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %d (%s), ",
	      yychar, yytname[yychar1]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to the semantic value of
     the lookahead token.  This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

#if YYLSP_NEEDED
  /* Similarly for the default location.  Let the user run additional
     commands if for instance locations are ranges.  */
  yyloc = yylsp[1-yylen];
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
#endif

#if YYDEBUG
  /* We have to keep this `#if YYDEBUG', since we use variables which
     are defined only if `YYDEBUG' is set.  */
  if (yydebug)
    {
      int yyi;

      YYFPRINTF (stderr, "Reducing via rule %d (line %d), ",
		 yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (yyi = yyprhs[yyn]; yyrhs[yyi] > 0; yyi++)
	YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
      YYFPRINTF (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif

  switch (yyn) {

case 1:
#line 115 "parser-rc.y"
{ current_line = 1; *current_cell = 0; org = NULL; }
    break;
case 2:
#line 117 "parser-rc.y"
{ root_line = yyvsp[0].line; }
    break;
case 5:
#line 126 "parser-rc.y"
{ yyval.line = yyvsp[0].line; }
    break;
case 6:
#line 129 "parser-rc.y"
{ 
		    if (yyvsp[0].line) 
		      { 
			struct line *end; 
			
			end = yyvsp[0].line; 
			while (end->next) 
			  end = end->next; 
			end->next = yyvsp[-1].line; 
			yyval.line = yyvsp[0].line; 
		      } 
		    else 
		      yyval.line = yyvsp[-1].line; 
		  }
    break;
case 7:
#line 145 "parser-rc.y"
{ root_line = yyvsp[-1].line; YYACCEPT; }
    break;
case 8:
#line 149 "parser-rc.y"
{ yyval.line = yyvsp[0].line; }
    break;
case 9:
#line 152 "parser-rc.y"
{ 
		    if (yyvsp[0].line) 
		      { 
			struct line *end; 
			
			end = yyvsp[0].line; 
			while (end->next) 
			  end = end->next; 
			end->next = yyvsp[-1].line; 
			yyval.line = yyvsp[0].line; 
		      } 
		    else 
		      yyval.line = yyvsp[-1].line; 
		  }
    break;
case 10:
#line 169 "parser-rc.y"
{ yyval.line = NULL; current_line++; }
    break;
case 11:
#line 172 "parser-rc.y"
{ yyval.line = yyvsp[-3].line; current_line++; for_index = NULL; }
    break;
case 14:
#line 181 "parser-rc.y"
{ yyval.line = NULL; }
    break;
case 15:
#line 184 "parser-rc.y"
{ yyval.line = NULL; }
    break;
case 16:
#line 187 "parser-rc.y"
{ yyval.line = yyvsp[0].line; }
    break;
case 17:
#line 190 "parser-rc.y"
{ yyval.line = malloc (sizeof (struct line)); yyval.line->next = NULL; yyval.line->cmd = yyvsp[0].cmd; (*current_cell)++; }
    break;
case 18:
#line 194 "parser-rc.y"
{
		    yyval.cmd = malloc (sizeof (struct rc_command));
		    if ((yyvsp[-3].integer & RC_CMD_MASK) == RC_CMD_DAT) /* ICWS 0251 */
		      {
			yyval.cmd->command = yyvsp[-3].integer | (yyvsp[-1].integer << RC_AM_A_TO_B);
			yyval.cmd->a.expr = NULL;
			yyval.cmd->b.expr = yyvsp[0].expr;
			yyval.cmd->command |= RC_AM_A_DIRECT;
		      }
		    else
		      {
			yyval.cmd->command = yyvsp[-3].integer | yyvsp[-1].integer;
			yyval.cmd->a.expr = yyvsp[0].expr;
			yyval.cmd->b.expr = NULL;
			yyval.cmd->command |= RC_AM_B_DIRECT;
		      }
		  }
    break;
case 19:
#line 213 "parser-rc.y"
{
		    yyval.cmd = malloc (sizeof (struct rc_command));
		    yyval.cmd->command = yyvsp[-7].integer | yyvsp[-5].integer | (yyvsp[-1].integer << RC_AM_A_TO_B);
		    yyval.cmd->a.expr = yyvsp[-4].expr;
		    yyval.cmd->b.expr = yyvsp[0].expr;
		  }
    break;
case 20:
#line 222 "parser-rc.y"
{}
    break;
case 22:
#line 228 "parser-rc.y"
{}
    break;
case 23:
#line 231 "parser-rc.y"
{ current_line++; }
    break;
case 24:
#line 234 "parser-rc.y"
{ current_line++; }
    break;
case 26:
#line 240 "parser-rc.y"
{ 
		    yyval.label = create_label(yyvsp[-1].name); 
		    if (!yyval.label)
		      YYERROR;
		    yyval.label->relative = 1; 
		    yyval.label->address = *current_cell; 
		    for_index = yyval.label; 
		  }
    break;
case 27:
#line 250 "parser-rc.y"
{ 
		    yyval.label = create_label(yyvsp[-2].name); 
		    if (!yyval.label)
		      YYERROR;
		    yyval.label->relative = 1; 
		    yyval.label->address = *current_cell; 
		    for_index = yyval.label;
		  }
    break;
case 28:
#line 261 "parser-rc.y"
{ yyval.integer = RC_AM_A_DIRECT; }
    break;
case 29:
#line 264 "parser-rc.y"
{ yyval.integer = RC_AM_A_IMMEDIATE; }
    break;
case 30:
#line 267 "parser-rc.y"
{ yyval.integer = RC_AM_A_DIRECT; }
    break;
case 31:
#line 270 "parser-rc.y"
{ yyval.integer = RC_AM_A_INDIRECT_B; }
    break;
case 32:
#line 273 "parser-rc.y"
{ yyval.integer = RC_AM_A_PRE_DEC_B; }
    break;
case 33:
#line 276 "parser-rc.y"
{ yyval.integer = RC_AM_A_POST_INC_B; }
    break;
case 34:
#line 279 "parser-rc.y"
{ yyval.integer = RC_AM_A_PRE_DEC_A; }
    break;
case 35:
#line 282 "parser-rc.y"
{ yyval.integer = RC_AM_A_POST_INC_A; }
    break;
case 36:
#line 285 "parser-rc.y"
{ yyval.integer = RC_AM_A_INDIRECT_A; }
    break;
case 37:
#line 289 "parser-rc.y"
{ yyval.expr = yyvsp[0].expr; }
    break;
case 38:
#line 292 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_EQU;
		    yyval.expr->a = yyvsp[-4].expr;
		    yyval.expr->b = yyvsp[0].expr;
		  }
    break;
case 39:
#line 301 "parser-rc.y"
{ yyval.expr = yyvsp[0].expr; }
    break;
case 40:
#line 304 "parser-rc.y"
{ yyval.expr = yyvsp[0].expr; }
    break;
case 41:
#line 308 "parser-rc.y"
{ yyval.expr = yyvsp[0].expr; }
    break;
case 42:
#line 311 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_ADD;
		    yyval.expr->integer = 1;
		    yyval.expr->a = yyvsp[-3].expr;
		    yyval.expr->b = yyvsp[0].expr;
		  }
    break;
case 43:
#line 320 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_SUB;
		    yyval.expr->integer = 1;
		    yyval.expr->a = yyvsp[-3].expr;
		    yyval.expr->b = yyvsp[0].expr;
		  }
    break;
case 44:
#line 330 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_ADD;
		    yyval.expr->integer = 1;
		    yyval.expr->a = yyvsp[-1].expr;
		    yyval.expr->b = yyvsp[0].expr;
		  }
    break;
case 45:
#line 339 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_ADD;
		    yyval.expr->integer = -1;
		    yyval.expr->a = yyvsp[-1].expr;
		    yyval.expr->b = yyvsp[0].expr;
		  }
    break;
case 46:
#line 348 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_SGN;
		    yyval.expr->integer = 1;
		    yyval.expr->a = yyvsp[0].expr;
		  }
    break;
case 47:
#line 356 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_SGN;
		    yyval.expr->integer = -1;
		    yyval.expr->a = yyvsp[0].expr;
		  }
    break;
case 48:
#line 365 "parser-rc.y"
{ yyval.expr = yyvsp[0].expr; }
    break;
case 49:
#line 368 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_MUL;
		    yyval.expr->a = yyvsp[-3].expr;
		    yyval.expr->b = yyvsp[0].expr;
		  }
    break;
case 50:
#line 376 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_DIV;
		    yyval.expr->a = yyvsp[-3].expr;
		    yyval.expr->b = yyvsp[0].expr;
		  }
    break;
case 51:
#line 384 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_MOD;
		    yyval.expr->a = yyvsp[-3].expr;
		    yyval.expr->b = yyvsp[0].expr;
		  }
    break;
case 52:
#line 393 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_LABEL;
		    yyval.expr->name = yyvsp[-1].name;
		  }
    break;
case 53:
#line 400 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_INTEGER;
		    yyval.expr->integer = yyvsp[-1].integer;
		  }
    break;
case 54:
#line 407 "parser-rc.y"
{ 
		    yyval.expr = cmalloc (sizeof (struct expr));
		    yyval.expr->what = EXPR_SGN;
		    yyval.expr->integer = 1;
		    yyval.expr->a = yyvsp[-2].expr;
		  }
    break;
case 55:
#line 417 "parser-rc.y"
{ if (org) free_expr (org); org = yyvsp[-3].expr; current_line++; }
    break;
case 56:
#line 420 "parser-rc.y"
{ current_line++; }
    break;
case 57:
#line 424 "parser-rc.y"
{ yyvsp[-3].label->relative = 0; yyvsp[-3].label->expr = yyvsp[0].expr; }
    break;
case 58:
#line 428 "parser-rc.y"
{ if (org) free_expr (org); org = yyvsp[0].expr; }
    break;
case 59:
#line 432 "parser-rc.y"
{ 
		    yyval.label = for_index;
		    if (for_index)
		      label_list = for_index->next;
		  }
    break;
case 60:
#line 438 "parser-rc.y"
{
		    int count;
		    jmp_buf env;

		    current_line++;
		    if (setjmp (env))
		      YYERROR;
		    else
		      count = eval_and_free_expr (yyvsp[-3].expr, *current_cell, env);
		    if (count<1 || count>RC_MAX_FOR_COUNT)
		      {
			snprintf (rc_error_message, 1024, "FOR count out off bounds in line %d.", current_line);
			rc_error_message[1023] = '\0';
			YYERROR;
		      }
		    yyval.integer = count;
		  }
    break;
case 61:
#line 457 "parser-rc.y"
{
		    struct line *start, *end, *current;
		    int i;
		    struct label *my_index;
		    int count;

		    count = yyvsp[-3].integer;
		    my_index = yyvsp[-10].label;
		    start = end = malloc (sizeof (struct line));
		    for (i=1; i<count; i++)
		      {
			current = yyvsp[-2].line;
			while (current)
			  {
			    end->next = malloc (sizeof (struct line));
			    end = end->next;
			    end->cmd = malloc (sizeof (struct rc_command));
			    end->cmd->command = current->cmd->command;
			    end->cmd->a.expr = copy_expr (current->cmd->a.expr);
			    end->cmd->b.expr = copy_expr (current->cmd->b.expr);
			    if (my_index!=NULL)
			      {
				subs_expr (end->cmd->a.expr, my_index, count-i+1);
				subs_expr (end->cmd->b.expr, my_index, count-i+1);
			      }
			    current = current->next;
			    (*current_cell)++;
			  }
		      }
		    if (my_index!=NULL)
		      {
			current = yyvsp[-2].line;
			while (current)
			  {
			    subs_expr (current->cmd->a.expr, my_index, 1);
			    subs_expr (current->cmd->b.expr, my_index, 1);
			    current = current->next;
			  }
		      }
		    end->next = yyvsp[-2].line;
		    yyval.line = start->next;
		    free (start);
		    if (my_index)
		      free (my_index);
		  }
    break;
}

#line 705 "/usr/share/bison/bison.simple"


  yyvsp -= yylen;
  yyssp -= yylen;
#if YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;
#if YYLSP_NEEDED
  *++yylsp = yyloc;
#endif

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  char *yymsg;
	  int yyx, yycount;

	  yycount = 0;
	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  for (yyx = yyn < 0 ? -yyn : 0;
	       yyx < (int) (sizeof (yytname) / sizeof (char *)); yyx++)
	    if (yycheck[yyx + yyn] == yyx)
	      yysize += yystrlen (yytname[yyx]) + 15, yycount++;
	  yysize += yystrlen ("parse error, unexpected ") + 1;
	  yysize += yystrlen (yytname[YYTRANSLATE (yychar)]);
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "parse error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[YYTRANSLATE (yychar)]);

	      if (yycount < 5)
		{
		  yycount = 0;
		  for (yyx = yyn < 0 ? -yyn : 0;
		       yyx < (int) (sizeof (yytname) / sizeof (char *));
		       yyx++)
		    if (yycheck[yyx + yyn] == yyx)
		      {
			const char *yyq = ! yycount ? ", expecting " : " or ";
			yyp = yystpcpy (yyp, yyq);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yycount++;
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exhausted");
	}
      else
#endif /* defined (YYERROR_VERBOSE) */
	yyerror ("parse error");
    }
  goto yyerrlab1;


/*--------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action |
`--------------------------------------------------*/
yyerrlab1:
  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;
      YYDPRINTF ((stderr, "Discarding token %d (%s).\n",
		  yychar, yytname[yychar1]));
      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;


/*-------------------------------------------------------------------.
| yyerrdefault -- current state does not do anything special for the |
| error token.                                                       |
`-------------------------------------------------------------------*/
yyerrdefault:
#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */

  /* If its default is to accept any token, ok.  Otherwise pop it.  */
  yyn = yydefact[yystate];
  if (yyn)
    goto yydefault;
#endif


/*---------------------------------------------------------------.
| yyerrpop -- pop the current state because it cannot handle the |
| error token                                                    |
`---------------------------------------------------------------*/
yyerrpop:
  if (yyssp == yyss)
    YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#if YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG
  if (yydebug)
    {
      short *yyssp1 = yyss - 1;
      YYFPRINTF (stderr, "Error: state stack now");
      while (yyssp1 != yyssp)
	YYFPRINTF (stderr, " %d", *++yyssp1);
      YYFPRINTF (stderr, "\n");
    }
#endif

/*--------------.
| yyerrhandle.  |
`--------------*/
yyerrhandle:
  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;
#if YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

/*---------------------------------------------.
| yyoverflowab -- parser overflow comes here.  |
`---------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}
#line 504 "parser-rc.y"


struct label *create_label(char *name)
{
  struct label * l;

  l = label_list;
  while (l)
    {
      if (strcmp (name, l->name)==0)
	{
	  snprintf (rc_error_message, 1024, "Duplicate label '%s' in line %d.", name, current_line);
	  rc_error_message[1023] = '\0';
	  return NULL;
	}
      l = l->next;
    }
  l = malloc (sizeof (struct label));
  l->next = label_list;
  label_list = l;
  l->name = name;
  l->in_eval = 0;
  return l;
}

void free_label_list()
{
  struct label *l, *l2;

  l = label_list;
  while (l)
    {
      l2 = l->next;
      free (l->name);
      free (l);
      l = l2;
    }
}

void free_expr(struct expr *expr)
{
  if (expr->name)
    free (expr->name);
  if (expr->a)
    free_expr (expr->a);
  if (expr->b)
    free_expr (expr->b);
  free (expr);
}

struct expr *copy_expr(struct expr *expr)
{
  struct expr *r;

  if (expr==NULL)
    return NULL;
  r = cmalloc (sizeof (struct expr));
  *r = *expr;
  if (r->name)
    r->name = strdup (r->name);
  if (r->a)
    r->a = copy_expr (r->a);
  if (r->b)
    r->b = copy_expr (r->b);
  return r;
}

void subs_expr(struct expr *expr, struct label *label, int i)
{
  struct expr *r;
  
  if (expr!=NULL)
    if (expr->what==EXPR_LABEL && strcmp (expr->name, label->name)==0)
      {
	expr->what = EXPR_INTEGER;
	expr->integer = i;
	free (expr->name);
	expr->name = NULL;
      }
    else
      {
	if (expr->a)
	  subs_expr (expr->a, label, i);
	if (expr->b)
	  subs_expr (expr->b, label, i);
      }
}

int eval_label(char *name, int rel, jmp_buf env)
{
  struct label *p;
  int i;

  p = label_list;
  while (p)
    {
      if (strcmp (name, p->name)==0)
	if (p->relative)
	  return p->address-rel;
	else
	  {
	    if (p->in_eval)
	      {
		snprintf (rc_error_message, 1024, "Label '%s' defined recursively.", name);
		rc_error_message[1023] = '\0';
		longjmp (env, -1);
	      }
	    p->in_eval = 1;
	    i = eval_expr (p->expr, rel, env);
	    p->in_eval = 0;
	    return i;
	  }
      p = p->next;
    }
  snprintf (rc_error_message, 1024, "Cannot find label '%s' to evaluate cell %d.", name, rel);
  rc_error_message[1023] = '\0';
  longjmp (env, -1);
}

int eval_expr(struct expr *expr, int rel, jmp_buf env)
{
  int d;

  switch (expr->what)
    {
    case  EXPR_INTEGER:
      return expr->integer;
      
    case EXPR_LABEL:
      return eval_label (expr->name, rel, env);

    case EXPR_ADD:
      return expr->integer * eval_expr(expr->a, rel, env) + eval_expr(expr->b, rel, env);

    case EXPR_SUB:
      return expr->integer * eval_expr(expr->a, rel, env) - eval_expr(expr->b, rel, env);

    case EXPR_SGN:
      return expr->integer * eval_expr(expr->a, rel, env);

    case EXPR_MUL:
      return eval_expr(expr->a, rel, env) * eval_expr(expr->b, rel, env);

    case EXPR_DIV:
      d = eval_expr(expr->b, rel, env);
      if (d==0)
	{
	  snprintf (rc_error_message, 1024, "Division by zero while evaluating cell %d.", rel);
	  rc_error_message[1023] = '\0';
	  longjmp (env, -1);
	}
      return eval_expr(expr->a, rel, env) / d;

    case EXPR_MOD:
      d = eval_expr(expr->b, rel, env);
      if (d==0)
	{
	  snprintf (rc_error_message, 1024, "Division by zero while evaluating cell %d.", rel);
	  rc_error_message[1023] = '\0';
	  longjmp (env, -1);
	}
      return eval_expr(expr->a, rel, env) % d;

    case EXPR_EQU:
      return eval_expr(expr->a, rel, env) == eval_expr(expr->b, rel, env);
    }
}

int eval_and_free_expr(struct expr *expr, int rel, jmp_buf env)
{
  int r;

  r = eval_expr (expr, rel, env);
  free_expr (expr);
  return r;
}

void add_constant(char *name, int value)
{
  struct label *l;

  l = create_label (name);
  l->relative = 0;
  l->expr = cmalloc (sizeof (struct expr));
  l->expr->what = EXPR_INTEGER;
  l->expr->integer = value;
}

char *command_names[] = {"DAT","MOV","ADD","SUB","MUL","DIV","MOD",
			 "JMP","JMZ","JMN","DJN","SEQ","SLT","SPL",
			 "SNE","NOP","IJZ"};
char *modifier_names[] = {"   ",".A ",".B ",".AB",".BA",".F ",".X ",".I "};
char mode_indicators[] = {' ','#','$','@','<','>','{','}','*'};

void rc_dump_instruction(struct rc_command *pr)
{
  printf ("%s",command_names[pr->command & RC_CMD_MASK]);
  printf ("%s ",modifier_names[(pr->command & RC_MOD_MASK)>>RC_MOD_SHIFT]);
  printf ("%c %6d, ",mode_indicators[(pr->command & RC_AM_A_MASK)>>RC_AM_A_SHIFT], 
	  pr->a.number);
  printf ("%c %6d\n",mode_indicators[(pr->command & RC_AM_B_MASK)>>RC_AM_B_SHIFT], 
	  pr->b.number);
}

void rc_dump_program(struct program *pr)
{
  int i;
  
  printf ("ORG %03d\n", pr->lang.rc.org);
  for (i=0; i<pr->lang.rc.size; i++)
    {
      printf ("%03d ", i);
      printf ("%s",command_names[pr->lang.rc.obj[i].command & RC_CMD_MASK]);
      printf ("%s ",modifier_names[(pr->lang.rc.obj[i].command & RC_MOD_MASK)>>RC_MOD_SHIFT]);
      printf ("%c %6d, ",mode_indicators[(pr->lang.rc.obj[i].command & RC_AM_A_MASK)>>RC_AM_A_SHIFT], 
	      pr->lang.rc.obj[i].a.number);
      printf ("%c %6d\n",mode_indicators[(pr->lang.rc.obj[i].command & RC_AM_B_MASK)>>RC_AM_B_SHIFT], 
	      pr->lang.rc.obj[i].b.number);
    }
}

void rc_dump_program_internal (struct program *pr)
{
  int i;
  
  printf ("%d\n", pr->lang.rc.size);
  printf ("%d\n", pr->lang.rc.org);
  for (i=0; i<pr->lang.rc.size; i++)
    {
      printf ("%d\n",pr->lang.rc.obj[i].command);
      printf ("%d\n",pr->lang.rc.obj[i].a.number);
      printf ("%d\n",pr->lang.rc.obj[i].b.number);
    }
}

int mod_matrix[][2][2] = {
  {{RC_MOD_I,RC_MOD_B},{RC_MOD_AB,RC_MOD_AB}},
  {{RC_MOD_F,RC_MOD_B},{RC_MOD_AB,RC_MOD_AB}},
  {{RC_MOD_B,RC_MOD_B},{RC_MOD_AB,RC_MOD_AB}}};  

int rc_load_program(struct program *pr)
{
  FILE *f;
  int i, j;
  struct line *p, *p2;
  struct rc_command *obj;
  int start;
  int ad, bd;
  int mod;
  jmp_buf env;
  char s[1024];
  char *sp;

  unload_program (pr);

  label_list = NULL;

  f = fopen (pr->filename, "r");
  if (!f)
    return FAIL;

  do 
    {
      if (fgets (s, 1024, f)==NULL)
	s[0] = '\0';
      else
	if (strncasecmp (s, ";name ", 6)==0)
	  {
	    s[strlen (s)-1] = '\0';
	    sp = s+6;
	    while (isspace (*sp))
	      sp++;
	    pr->title = strdup (sp);
	  }
	else if (strncasecmp (s, ";author ", 8)==0)
	  {
	    s[strlen (s)-1] = '\0';
	    sp = s+8;
	    while (isspace (*sp))
	      sp++;
	    pr->author = strdup (sp);
	  }
    }
  while (s[0]==';' || isspace (s[0]));
  if (pr->title==NULL)
    pr->title = strdup (pr->filename);
  if (pr->author==NULL)
    pr->author = strdup ("unknown");
  rewind (f);

  add_constant (strdup ("CORESIZE"), SIZE);
  add_constant (strdup ("MAXPROCESSES"), MAX_THREADS);
  add_constant (strdup ("MAXCYCLES"), MAX_CYCLES);
  add_constant (strdup ("MAXLENGTH"), MAX_LENGTH);
  add_constant (strdup ("MINDISTANCE"), MIN_DISTANCE);
  add_constant (strdup ("CURLINE"), 0);
  current_cell = &label_list->expr->integer;

  rc_restart (f);
  rc_error_message[0] = '\0';
  if (rc_parse())
    {
      fclose (f);
      if (rc_error_message[0]=='\0')
	{
	  snprintf (rc_error_message, 1024, "%s (%d): Syntax error", pr->filename, current_line);
	  rc_error_message[1023] = '\0';
	}
      pr->error = strdup (rc_error_message);
      return OK;
    }
  fclose (f);

  obj = malloc ((*current_cell)*sizeof (struct rc_command));
  i = *current_cell;
  p = root_line;
  while (p)
    {
      obj[--i] = *p->cmd;
      p2 = p->next;
      free (p);
      p = p2;
    }
  
  if ((*current_cell)>MAX_LENGTH)
    {
      for (j=0; j<(*current_cell); j++)
	{
	  if (obj[j].a.expr)
	    free_expr (obj[j].a.expr);
	  if (obj[j].b.expr)
	    free_expr (obj[j].b.expr);
	}
      free_label_list();
      free (obj);
      snprintf (rc_error_message, 1024, "%s: Program exceeds maximum length (by %d instructions)", 
		pr->filename, (*current_cell)-MAX_LENGTH);
      rc_error_message[1023] = '\0';
      pr->error = strdup (rc_error_message);
      return OK;
    }

  for (i=0; i<(*current_cell); i++)
    {
      if (setjmp (env))
	{
	  if (obj[i].b.expr)
	    free_expr (obj[i].b.expr);
	  for (j=i+1; j<(*current_cell); j++)
	    {
	      if (obj[j].a.expr)
		free_expr (obj[j].a.expr);
	      if (obj[j].b.expr)
		free_expr (obj[j].b.expr);
	    }
	  free_label_list();
	  free (obj);
	  pr->error = strdup (rc_error_message);
	  return OK;
	}
      else
	if (obj[i].a.expr)
	  obj[i].a.number = eval_and_free_expr (obj[i].a.expr, i, env);
      if (setjmp (env))
	{
	  for (j=i+1; j<(*current_cell); j++)
	    {
	      if (obj[j].a.expr)
		free_expr (obj[j].a.expr);
	      if (obj[j].b.expr)
		free_expr (obj[j].b.expr);
	    }
	  free_label_list();
	  free (obj);
	  pr->error = strdup (rc_error_message);
	  return OK;
	}
      else
	if (obj[i].b.expr)
	  obj[i].b.number = eval_and_free_expr (obj[i].b.expr, i, env);
      if ((obj[i].command & RC_MOD_MASK) == RC_MOD_NONE)
	{
	  ad = ((obj[i].command & RC_AM_A_MASK) == RC_AM_A_IMMEDIATE);
	  bd = ((obj[i].command & RC_AM_B_MASK) == RC_AM_B_IMMEDIATE);
	  switch (obj[i].command & RC_CMD_MASK)
	    {
	    case RC_CMD_DAT:
	    case RC_CMD_NOP:
	      mod = RC_MOD_F;
	      break;
	      
	    case RC_CMD_MOV:
	    case RC_CMD_SEQ:
	    case RC_CMD_SNE:
	      mod = mod_matrix[0][ad][bd];
	      break;

	    case RC_CMD_ADD:
	    case RC_CMD_SUB:
	    case RC_CMD_MUL:
	    case RC_CMD_DIV:
	    case RC_CMD_MOD:
	      mod = mod_matrix[1][ad][bd];
	      break;

	    case RC_CMD_SLT:
	      mod = mod_matrix[2][ad][bd];
	      break;
	      
	    case RC_CMD_JMP:
	    case RC_CMD_JMZ:
	    case RC_CMD_JMN:
	    case RC_CMD_DJN:
	    case RC_CMD_IJZ:
	    case RC_CMD_SPL:
	      mod = RC_MOD_B;
	      break;
	    }
	  obj[i].command |= mod;
	}
    }

  if (org)
    if (setjmp (env))
      {
	free_label_list();
	free (obj);
	pr->error = strdup (rc_error_message);
	return OK;
      }
    else
      start = eval_and_free_expr (org, 0, env);
  else
    start = 0;

  pr->lang.rc.obj = obj;
  pr->lang.rc.org = start;
  pr->lang.rc.size = *current_cell;
  return OK;
}

void rc_free_program(struct program *pr)
{
  if (pr->lang.rc.obj)
    free (pr->lang.rc.obj);
  pr->lang.rc.obj = NULL;
}

void rc_error (char *s)
{
}
