#ifndef BISON_Y_TAB_H
# define BISON_Y_TAB_H

#ifndef YYSTYPE
typedef union {
  int integer;
  char *name;
  struct rc_command *cmd;
  struct line *line;
  struct label *label;
  struct expr *expr;
} yystype;
# define YYSTYPE yystype
# define YYSTYPE_IS_TRIVIAL 1
#endif
# define	INTEGER	257
# define	IDENTIFIER	258
# define	WHITESPACE	259
# define	COMMENT	260
# define	RC_CMD	261
# define	RC_INSTR_EQU	262
# define	RC_INSTR_ORG	263
# define	RC_INSTR_END	264
# define	RC_INSTR_FOR	265
# define	RC_INSTR_ROF	266


extern YYSTYPE rc_lval;

#endif /* not BISON_Y_TAB_H */
