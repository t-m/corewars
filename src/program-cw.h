/*  Core Wars.
 *  Copyright (C) 1999  Walter Hofmann
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as 
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef __PROGRAM_CW_H__
#define __PROGRAM_CW_H__

#include "program.h"

#define AM_ADDRESS	0
#define AM_CONSTANT	1
#define AM_VALUE	2
#define AM_INDIRECT	3
#define AM_STRING	4

#define CMD_TITLE	0
#define CMD_AUTHOR	1
#define CMD_DATA	2
#define CMD_MOVE	3
#define CMD_ADD		4
#define CMD_NEG		5
#define CMD_AND		6
#define CMD_NOT		7
#define CMD_MUL		8
#define CMD_DIV		9
#define CMD_MOD		10
#define CMD_EQUAL	11
#define CMD_LESS	12
#define CMD_JUMP	13
#define CMD_FORK	14
#define CMD_INFO	15
#define CMD_SYSTEM	16
#define CMD_OWN		17
#define CMD_LOOP	18
#define CMD_MOVEI	19

struct cw_operand {
  int accessmode;
  union {
    int value;
    char *name;
  } operand;
} ;

struct cw_line {
  struct cw_line *next;
  int line_number;
  char *label;
  int command;
  struct cw_operand *operand1, *operand2;
} ;

extern char cw_error_message[1024];

int cw_load_program (struct program *p);
void cw_free_program (struct program *p);
void rc_dump_program (struct program *pr);
void rc_dump_program_internal (struct program *pr);

#endif
