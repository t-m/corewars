/*  Core Wars.
 *  Copyright (C) 1999  Walter Hofmann
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as 
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef __PSTATISTIC_H__
#define __PSTATISTIC_H__

#ifndef CMD_LINE
#define PSTATISTIC_WINDOW_HEIGHT 200
#define PSTATISTIC_TITLE_WIDTH 140
#define PSTATISTIC_AUTHOR_WIDTH 220

void pstatistic_view ();
void pstatistic_update ();
void pstatistic_clear ();
#endif

int program_compare (const void *a, const void *b);
void pstatistic_add ();

#endif
